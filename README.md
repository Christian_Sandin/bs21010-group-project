
The aim of this program is to automate the process of using a spectrophotometer to measure the motility of Brine shrimp or other aquatic organisms of a similar size.

In order to run the program the files;

    *PCS_Brine_Shrimp.py
    *J6305.py

Must be downloaded from the source.

Once downloaded run the program "PCS_Brine_Shrimp.py" from the command lines and then enter the information asked for by the program