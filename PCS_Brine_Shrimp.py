
from J6305 import Spectrometer
from time import gmtime, strftime
from datetime import datetime, timedelta

class brine_shrimp():
    
    #import spectrometer stuff
    def __init__(self):
        self.spec = Spectrometer()
    

    #calibrate spec...
    def setup(self):
        self.spec.set_wavelength(480)
        self.spec.calibrate()
        
    
    def scan_blips(self, thresh = 0.002): 	#sets the function along with the thresh hold value
        blips = 0
        blip_flag = 0
        begin = input("Please insert the brine shrimp, and type enter ").upper()	#ensure the program will only start when the user declares
        if begin == "ENTER":
            timenow = datetime.now()	#Finds the time at the beginning of the measurements.
            gap = int(input("How many minutes do you want to run for? "))   #Allows the length of the measurement period to be set.
            timegap = timedelta(minutes = gap)	
            stoptime = timenow + timegap	#determines the end time of experiment 
            start_abs = int(self.spec.absorbance()[0])	#finds the start absorbance of sample
            while timenow < stoptime:	#continues looping as long as it is before end time 
                timenow = datetime.now()	#constantly updating current time so to prevent an infinite loop
                cur_abs = int(self.spec.absorbance()[0])
                diff = cur_abs - start_abs	# checks that the current absorbance is above the thresh-hold
                if diff > thresh:	#records blips if abs is above the thresh-hold 
                    if blip_flag ==0:
                        blips = blips +1
                        blip_flag = 1 # prevents continuous recording of blips 
                else:
                    blip_flag = 0
            print("The number of brine shrimp blips are: " , blips)	#presents to the user the final no. of blips 
                
                
            
            
bs = brine_shrimp()             
bs.scan_blips()